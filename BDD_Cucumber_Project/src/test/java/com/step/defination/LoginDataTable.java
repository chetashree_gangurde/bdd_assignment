package com.step.defination;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginDataTable {
	WebDriver driver = null;

	//implemented Hook concept and data table
	@Before
	public void setUp() {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\chetashree.local\\Downloads\\Cucumber Infostretch Project\\CucumberTestProject\\1Cucumber_Test1\\src\\test\\resources\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Given("^I am on a login page$")
	public void I_am_on_a_login_page() {

		driver.manage().window().maximize();
		driver.get("https://nest.infostretch.com/");
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
	}

	@When("^I enter valid data on the page$")
	public void I_enter_valid_data_on_the_page(DataTable table) {
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		List<List<String>> data = table.raw();
		driver.findElement(By.xpath("//input[@id='username']")).sendKeys(data.get(0).get(0));
		driver.findElement(By.xpath("//input[@id='password']")).sendKeys(data.get(0).get(1));
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		System.out.println(driver.getCurrentUrl());
	}

	@Then("^The user login should be successful$")
	public void The_user_login_should_be_successful() {
		if (driver.getCurrentUrl().equalsIgnoreCase("https://nest.infostretch.com/#/login")) {
			System.out.println("Test Case Passed");
		} else {
			System.out.println("Test Case Failed");
		}
	}

	@After
	public void cleanUp() {
		driver.close();
	}
}
