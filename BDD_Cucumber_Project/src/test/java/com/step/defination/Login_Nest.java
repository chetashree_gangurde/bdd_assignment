package com.step.defination;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class Login_Nest {

	WebDriver driver=null;

	@Given("^User is on login page$")
	public void user_is_on_login_page() {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\chetashree.local\\Downloads\\Cucumber Infostretch Project\\CucumberTestProject\\1Cucumber_Test1\\src\\test\\resources\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://nest.infostretch.com/");
	}

	@When("^Title of web page is Infostretch Nest$")
	public void title_of_web_page_is_Infostretch_Nest() {
		String title = driver.getTitle();
		System.out.println(title);
		Assert.assertEquals("Infostretch NEST", title);
	}

	@When("^user enters username and password$")
	public void user_enters_username_and_password() {
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//input[@id='username']")).sendKeys("chetashree.gangurde");
		driver.findElement(By.xpath("//input[@id='password']")).sendKeys("Mastermind@20");
	}

	@When("^user clicks  on login button$")
	public void user_clicks_on_login_button() {
		driver.findElement(By.xpath("//button[@type='submit']")).click();
	}

	@Then("^user is on home page$")
	public void user_is_on_home_page() {
		String title = driver.getTitle();
		System.out.println(title);
		driver.quit();
	}

}
