package com.step.defination;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

//implemented Scenario outline concept
public class Login_With_Scenario_Outline {
	WebDriver driver = null;

	@Given("^User is on login page of Infostretch$")
	public void user_is_on_login_page_of_Infostretch() {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\chetashree.local\\Downloads\\Cucumber Infostretch Project\\CucumberTestProject\\1Cucumber_Test1\\src\\test\\resources\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://nest.infostretch.com/");
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
	}

	@When("^Title of web page is Infostretch$")
	public void title_of_web_page_is_Infostretch() {
		String title = driver.getTitle();
		System.out.println(title);
		Assert.assertEquals("Infostretch NEST", title);
	}

	@When("^I enter Username as \"([^\"]*)\" and Password as \"([^\"]*)\"$")
	public void I_enter_Username_as_and_Password_as(String arg1, String arg2) {
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//input[@id='username']")).sendKeys(arg1);
		driver.findElement(By.xpath("//input[@id='password']")).sendKeys(arg2);
		driver.findElement(By.xpath("//button[@type='submit']")).click();
	}

	@Then("^login should be unsuccessful$")
	public void login_should_be_unsuccessful() {
		if (driver.getCurrentUrl().equalsIgnoreCase("https://nest.infostretch.com/#/myview")) {
			System.out.println("Test Case Failed");
		} else {
			System.out.println("Test Case Passed");
		}
		driver.close();
	}

}
