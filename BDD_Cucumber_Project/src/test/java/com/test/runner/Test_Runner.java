package com.test.runner;
import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

//Used concept: Cucumber options: Features, Step-Definition, format- pretty plugin, Tags

@RunWith(Cucumber.class)
@CucumberOptions(
		 features = "C:\\Users\\chetashree.local\\Downloads\\Cucumber Infostretch Project\\CucumberTestProject\\1Cucumber_Test1\\src\\test\\java\\com\\feature\\files\\LoginDataTable.Feature"
		 , glue = {"com.step.defination"},
		 format = {"pretty","html:test-output"}, dryRun = false, tags = {"@SmokeTest"}
		)

public class Test_Runner {
			
			
}


