$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/chetashree.local/Downloads/Cucumber Infostretch Project/CucumberTestProject/1Cucumber_Test1/src/test/java/com/feature/files/LoginDataTable.Feature");
formatter.feature({
  "line": 2,
  "name": "Login Using Data Table",
  "description": "",
  "id": "login-using-data-table",
  "keyword": "Feature"
});
formatter.before({
  "duration": 5804385000,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "Login to Site",
  "description": "",
  "id": "login-using-data-table;login-to-site",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@SmokeTest"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "I am on a login page",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "I enter valid data on the page",
  "rows": [
    {
      "cells": [
        "username",
        "chetashree.gangurde"
      ],
      "line": 8
    },
    {
      "cells": [
        "password",
        "Mastermind@20"
      ],
      "line": 9
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "The user login should be successful",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginDataTable.I_am_on_a_login_page()"
});
formatter.result({
  "duration": 24326021200,
  "status": "passed"
});
formatter.match({
  "location": "LoginDataTable.I_enter_valid_data_on_the_page(DataTable)"
});
formatter.result({
  "duration": 1450547700,
  "status": "passed"
});
formatter.match({
  "location": "LoginDataTable.The_user_login_should_be_successful()"
});
formatter.result({
  "duration": 11472800,
  "status": "passed"
});
formatter.after({
  "duration": 328738000,
  "status": "passed"
});
});